const { Router } = require('express')
const route = Router()

const usuariosCtrl = require('../controllers/usuarios.controller')

route.get('/', usuariosCtrl.getUsuarios)
route.get('/:id', usuariosCtrl.getUsuario)
route.put('/:id', usuariosCtrl.editUsuario)
route.post('/', usuariosCtrl.createUsuario)
route.delete('/:id', usuariosCtrl.deleteUsuario)




module.exports = route