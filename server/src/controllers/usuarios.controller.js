
const usuarioCtrl = {}
const modelUsuario = require('../models/usuario')


usuarioCtrl.getUsuarios = async (req, res) => {
    const Usuarios = await modelUsuario.find()
    res.json (Usuarios)
}
usuarioCtrl.getUsuario = async (req, res) => {
    const Usuario = await modelUsuario.findOne({_id: req.params.id})
    //const Usuario = await modelUsuario.findById(req.body)
    res.json (Usuario)
}
usuarioCtrl.createUsuario = async (req, res) => {
    const newUsuario = new modelUsuario(req.body)
    await newUsuario.save()
    res.send({message:"Usuario creado"})
}
usuarioCtrl.editUsuario = async (req, res) => {
    const Usuario = await modelUsuario.findByIdAndUpdate(req.params.id, req.body)
    //const Usuario = await modelUsuario.findOneAndUpdate({_id: req.params.id, body: req.body})
    res.json ({status: 'ok', message: Usuario, body: req.body})
}
usuarioCtrl.deleteUsuario = async (req, res) => {
    const Usuario = await modelUsuario.findOneAndDelete({_id: req.params.id})
    res.json ({status: 'ok', message: 'Usuario borrado'})
}


module.exports = usuarioCtrl